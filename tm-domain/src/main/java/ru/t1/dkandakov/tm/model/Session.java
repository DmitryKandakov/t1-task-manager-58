package ru.t1.dkandakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.exception.user.UserNotFoundException;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Cacheable
@Table(name = "tm_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractUserOwnedModel {

    @NotNull
    @Column(name = "date")
    private Date date = new Date();

    @Nullable
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = null;

    public Session(@NotNull final User user) {
        if (user == null) throw new UserNotFoundException();
        this.setUser(user);
    }

}