package ru.t1.dkandakov.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public class NumberIncorrectException extends AbstractFildException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(@NotNull final String value) {
        super("Error! Value\"" + value + "\"is incorrect...");
    }

    public NumberIncorrectException(@NotNull final String value, final Throwable cause) {
        super("Error! Value\"" + value + "\"is incorrect...");
    }

}
