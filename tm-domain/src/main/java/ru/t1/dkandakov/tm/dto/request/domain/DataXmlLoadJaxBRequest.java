package ru.t1.dkandakov.tm.dto.request.domain;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataXmlLoadJaxBRequest extends AbstractUserRequest {

    public DataXmlLoadJaxBRequest(@Nullable String token) {
        super(token);
    }

}