package ru.t1.dkandakov.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectClearResponse extends AbstractProjectResponse {

    public ProjectClearResponse(@Nullable ProjectDTO project) {
        super(project);
    }
}
