package ru.t1.dkandakov.tm.api.service.model;

import ru.t1.dkandakov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
