package ru.t1.dkandakov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, created, name, description, status, user_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId});")
    void add(@Nullable final ProjectDTO model);

    @Delete("TRUNCATE TABLE tm_project;")
    void clearAll();

    @Select("SELECT * FROM tm_project;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull List<ProjectDTO> findAllProjects();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY #{sort};")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @NotNull List<ProjectDTO> findAll(@NotNull @Param("userId") String userId, @NotNull @Param("sort") String sort);

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1;")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    @Nullable ProjectDTO findOneByIdByUserId(@NotNull @Param("userId") String userId, @Param("id") @NotNull String id);

    @Select("SELECT * FROM tm_project WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable ProjectDTO findOneById(@NotNull final String id);

    @Update("UPDATE tm_project SET created = #{created}, name = #{name}, description = #{description}, status = #{status}, user_id = #{userId} WHERE id = #{id};")
    void update(@Param("userId") @Nullable String userId, @NotNull ProjectDTO model);

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{userId};")
    int getSizeByUserId(@Param("userId") @Nullable String userId);

    @Select("SELECT COUNT(1) FROM tm_project")
    int getSize();

    @Nullable
    @Delete("DELETE FROM tm_task WHERE id = #{id} and user_id = #{userId};")
    void removeOneByIdByUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);


    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void removeAllByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT COUNT(1) = 1 FROM tm_project WHERE id = #{id} AND user_id = #{userId};")
    Boolean existsByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<ProjectDTO> findAllByUserId(@NotNull final String userId);

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void removeOneById(@NotNull final String id);

}