package ru.t1.dkandakov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.repository.model.IUserRepository;
import ru.t1.dkandakov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<User> findAll() {
        @NotNull final String jpql = "SELECT m FROM User m";
        return entityManager.createQuery(jpql, User.class).getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("login", login)
                .setHint("org.hibernate.cacheable", true)
                .getSingleResult();
    }

    @Nullable
    public User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("email", email)
                .setHint("org.hibernate.cacheable", true)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        Optional<User> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }


    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM User m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

}
