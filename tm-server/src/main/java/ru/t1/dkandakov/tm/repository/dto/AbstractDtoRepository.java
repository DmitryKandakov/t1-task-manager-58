package ru.t1.dkandakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.dkandakov.tm.api.repository.dto.IRepositoryDTO;
import ru.t1.dkandakov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    @Autowired
    protected final EntityManager entityManager;

    protected AbstractDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
