package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.SessionDTO;
import ru.t1.dkandakov.tm.dto.model.UserDTO;

public interface IAuthService {

    @Nullable
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable SessionDTO session);

    @NotNull
    UserDTO check(@Nullable String login, @Nullable String password);

}