package ru.t1.dkandakov.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.dkandakov.tm.api.service.IDatabaseProperty;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;
import ru.t1.dkandakov.tm.dto.model.SessionDTO;
import ru.t1.dkandakov.tm.dto.model.TaskDTO;
import ru.t1.dkandakov.tm.dto.model.UserDTO;
import ru.t1.dkandakov.tm.model.Project;
import ru.t1.dkandakov.tm.model.Session;
import ru.t1.dkandakov.tm.model.Task;
import ru.t1.dkandakov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.t1.dkandakov.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull IDatabaseProperty databaseProperty) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, databaseProperty.getDatabaseLogin());
        settings.put(org.hibernate.cfg.Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHBM2DDLAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseProperty.getDatabaseShowSQL());
        settings.put(org.hibernate.cfg.Environment.FORMAT_SQL, databaseProperty.getDatabaseFormatSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseSecondLvlCash());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseQueryCashKey());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseConfigFilePath());
        settings.put(Environment.FORMAT_SQL, databaseProperty.getDatabaseFormatSql());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}