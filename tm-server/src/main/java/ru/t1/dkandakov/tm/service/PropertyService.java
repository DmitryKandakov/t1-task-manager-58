package ru.t1.dkandakov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.dkandakov.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['server.port']}")
    private Integer serverPort;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @NotNull
    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @NotNull
    @Value("#{environment['database.username']}")
    private String databaseLogin;

    @NotNull
    @Value("#{environment['database.password']}")
    private String databasePassword;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHBM2DDLAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String databaseShowSQL;

    @NotNull
    @Value("#{environment['database.second_lvl_cash']}")
    private String databaseSecondLvlCash;

    @NotNull
    @Value("#{environment['database.factory_class']}")
    private String databaseFactoryClass;

    @NotNull
    @Value("#{environment['database.use_query_cash']}")
    private String databaseQueryCashKey;

    @NotNull
    @Value("#{environment['database.use_min_puts']}")
    private String databaseUseMinPuts;

    @NotNull
    @Value("#{environment['database.region_prefix']}")
    private String databaseRegionPrefix;

    @NotNull
    @Value("#{environment['database.config_file_path']}")
    private String databaseConfigFilePath;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    private String DatabaseFormatSql;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

}
