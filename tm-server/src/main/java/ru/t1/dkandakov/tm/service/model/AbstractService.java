package ru.t1.dkandakov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.t1.dkandakov.tm.api.repository.model.IRepository;
import ru.t1.dkandakov.tm.api.service.model.IService;
import ru.t1.dkandakov.tm.exception.entity.EntityNotFoundException;
import ru.t1.dkandakov.tm.exception.entity.ValueIsNullException;
import ru.t1.dkandakov.tm.exception.field.IdEmptyException;
import ru.t1.dkandakov.tm.exception.field.IndexIncorrectException;
import ru.t1.dkandakov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }


    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final EntityManager entityManager);

    @NotNull
    @Override
    public M add(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@Nullable Collection<M> models) {
        if (models == null) throw new ValueIsNullException();

        for (final M model : models) {
            add(model);
        }

        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models == null) throw new ValueIsNullException();

        removeAll();
        add(models);

        return models;
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository repository = getRepository(entityManager);
            return repository.findAll();
        } catch (@NotNull final NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository repository = getRepository(entityManager);
            return (M) repository.findOneById(id);
        } catch (@NotNull final NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();

        @NotNull final List<M> models = findAll();
        return models.get(index);
    }

    @NotNull
    @Override
    public M removeOne(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable M model = findOneById(id);
        if (model == null) return null;

        return removeOne(model);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();

        @Nullable M model = findOneByIndex(index);
        if (model == null) return null;

        return removeOne(model);
    }


    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository repository = getRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(String id) {
        if (id == null || id.isEmpty()) return false;
        return findOneById(id) != null;
    }

}
