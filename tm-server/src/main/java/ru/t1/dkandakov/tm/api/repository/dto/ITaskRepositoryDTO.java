package ru.t1.dkandakov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.TaskDTO;
import ru.t1.dkandakov.tm.enumerated.TaskSort;

import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnerRepositoryDTO<TaskDTO> {

    @Nullable
    List<TaskDTO> findAll(@NotNull String userId, @NotNull TaskSort sort);

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeTasksByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

}
