package ru.t1.dkandakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModelDTO> extends IRepository<M> {

    void removeAll(@NotNull String userId);

    boolean existsById(@NotNull String UserId, @NotNull String Id);

    @Nullable
    List<M> findAll(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String useId, @NotNull String Id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    int getSize(@NotNull String userId);

    @Nullable
    M removeOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    M removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    M add(final @NotNull String UserId, @NotNull M model);

    @Nullable
    M removeOne(final @NotNull String userId, @NotNull M model);

}