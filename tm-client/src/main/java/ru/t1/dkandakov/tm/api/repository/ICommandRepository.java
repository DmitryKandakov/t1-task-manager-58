package ru.t1.dkandakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.listener.AbstractListener;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractListener command);

    @Nullable
    AbstractListener getCommandByArgument(@Nullable String argument);

    @Nullable
    AbstractListener getCommandByName(@Nullable String name);

    @NotNull
    Collection<AbstractListener> getTerminalCommands();

    @NotNull
    Iterable<AbstractListener> getCommandsWithArgument();

}