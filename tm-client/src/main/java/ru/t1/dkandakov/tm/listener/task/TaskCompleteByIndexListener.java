package ru.t1.dkandakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.task.TaskCompleteByIndexRequest;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIndexListener extends AbstractTaskListener {

    @Override
    @EventListener(condition = "@taskCompleteByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskCompleteByIndexRequest request =
                new TaskCompleteByIndexRequest(getToken(), index, Status.COMPLETED);
        getTaskEndpoint().completeTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by index.";
    }

}