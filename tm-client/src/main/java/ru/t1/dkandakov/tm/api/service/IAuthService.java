package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.UserDTO;
import ru.t1.dkandakov.tm.enumerated.Role;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    UserDTO getUser();

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    UserDTO check(@Nullable String login, @Nullable String password);

}