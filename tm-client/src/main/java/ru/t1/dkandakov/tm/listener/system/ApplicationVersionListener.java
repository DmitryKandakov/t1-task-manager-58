package ru.t1.dkandakov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show version.";
    }

}