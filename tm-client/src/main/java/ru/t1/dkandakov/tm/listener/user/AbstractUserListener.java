package ru.t1.dkandakov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.api.endpoint.IUserEndpoint;
import ru.t1.dkandakov.tm.listener.AbstractListener;

@Component

public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}


