package ru.t1.dkandakov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.domain.DataBase64LoadRequest;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.event.ConsoleEvent;

@Component
public class DataBase64LoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-base64";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64LoadListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA BASE64 LOAD]");
        getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from a base64 file.";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}