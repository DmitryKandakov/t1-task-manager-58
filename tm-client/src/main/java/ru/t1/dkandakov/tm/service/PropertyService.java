package ru.t1.dkandakov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.dkandakov.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['server.host']}")
    private String host;

    @NotNull
    @Value("#{environment['server.port']}")
    private String port;

    @NotNull
    @Value("#{environment['admin.login']}")
    private String adminLogin;

    @NotNull
    @Value("#{environment['admin.password']}")
    private String adminPassword;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read("buildNumber");
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read("email");
    }

    @Override
    public @NotNull Integer getPasswordIteration() {
        return null;
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read("developer");
    }


}
