package ru.t1.dkandakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIndexListener extends AbstractTaskListener {

    @Override
    @EventListener(condition = "@taskUpdateByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request =
                new TaskUpdateByIndexRequest(getToken(), index, name, description);
        getTaskEndpoint().updateTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-update-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task by index.";
    }

}