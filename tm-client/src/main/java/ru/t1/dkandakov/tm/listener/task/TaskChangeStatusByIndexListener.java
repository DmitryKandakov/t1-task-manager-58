package ru.t1.dkandakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskChangeStatusByIndexListener extends AbstractTaskListener {

    @Override
    @EventListener(condition = "@taskChangeStatusByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIndexRequest request =
                new TaskChangeStatusByIndexRequest(getToken(), index, status);
        getTaskEndpoint().changeTaskStatusByIndexRequest(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-update-status-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change task's status by index.";
    }

}