package ru.t1.dkandakov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class ProjectCompleteByIndexListener extends AbstractProjectListener {

    @Override
    @EventListener(condition = "@projectCompleteByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectCompleteByIndexRequest request =
                new ProjectCompleteByIndexRequest(getToken(), index, Status.COMPLETED);
        getProjectEndpoint().completeProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by index.";
    }

}