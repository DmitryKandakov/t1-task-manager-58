package ru.t1.dkandakov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.domain.DataJsonSaveJaxBRequest;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.event.ConsoleEvent;

@Component
public class DataJsonSaveJaxBListener extends AbstractDataListener {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in json file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonSaveJaxBListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE JSON]");
        getDomainEndpoint().saveDataJsonJaxb(new DataJsonSaveJaxBRequest(getToken()));
    }


    @NotNull
    @Override
    public String getName() {
        return "data-save-json-jaxb";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}