package ru.t1.dkandakov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.listener.AbstractListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    @Autowired
    private final Bootstrap bootstrap;
    @NotNull
    private final List<String> commands = new ArrayList<>();
    @NotNull
    private final File folder = new File("./");
    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        @NotNull final Iterable<AbstractListener> commands = listeners;
        commands.forEach(e -> this.commands.add(e.getName()));


        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    public void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @Nullable final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                } catch (@Nullable Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void start() {
        init();
    }

    public void stop() {
        es.shutdown();
    }

}