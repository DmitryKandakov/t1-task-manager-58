package ru.t1.dkandakov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.api.service.IPropertyService;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.listener.AbstractListener;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    public IPropertyService propertyService;
    @NotNull
    @Autowired
    protected AbstractListener[] listeners;

    @NotNull
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return propertyService;
    }

}