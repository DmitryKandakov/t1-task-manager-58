package ru.t1.dkandakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModelDTO> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @Nullable
    List<M> findAll();

    @Nullable
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    @Nullable
    M removeOne(@NotNull M model);

    @Nullable
    M removeOneById(@NotNull String id);

    @Nullable
    M removeOneByIndex(@NotNull Integer index);

    void removeAll(Collection<M> collection);

    void clear();

    int getSize();

    boolean existsById(String id);

}