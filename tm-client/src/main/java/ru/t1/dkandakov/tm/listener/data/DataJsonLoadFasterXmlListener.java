package ru.t1.dkandakov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.domain.DataJsonLoadFasterXmlRequest;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.event.ConsoleEvent;

@Component
public class DataJsonLoadFasterXmlListener extends AbstractDataListener {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Load data from json file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonLoadFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD JSON]");
        getDomainEndpoint().loadDataJsonFasterXml(new DataJsonLoadFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return "data-load-json";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}