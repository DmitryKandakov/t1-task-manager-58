package ru.t1.dkandakov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.user.UserRemoveRequest;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class UserRemoveListener extends AbstractUserListener {

    @NotNull
    private final String NAME = "user-remove";

    @NotNull
    private final String DESCRIPTION = "remove user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userRemoveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken(), login);
        getUserEndpoint().removeUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}