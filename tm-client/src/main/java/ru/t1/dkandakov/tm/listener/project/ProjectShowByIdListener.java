package ru.t1.dkandakov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.project.ProjectGetByIdRequest;
import ru.t1.dkandakov.tm.dto.response.project.ProjectGetByIdResponse;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken(), id);
        @NotNull final ProjectGetByIdResponse response = getProjectEndpoint().getProjectById(request);
        showProject(response.getProject());
    }

    @Nullable
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by id.";
    }

}