package ru.t1.dkandakov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;
import ru.t1.dkandakov.tm.dto.request.project.ProjectRemoveByIndexRequest;
import ru.t1.dkandakov.tm.dto.response.project.ProjectRemoveByIndexResponse;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIndexListener extends AbstractProjectListener {

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectRemoveByIndexRequest request =
                new ProjectRemoveByIndexRequest(getToken(), index);
        @NotNull final ProjectRemoveByIndexResponse response = getProjectEndpoint().removeProjectByIndex(request);
        @Nullable final ProjectDTO project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

}